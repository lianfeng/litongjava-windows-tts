/*
SQLyog Ultimate v8.32 
MySQL - 5.5.5-10.4.11-MariaDB : Database - litongjava_windows_tts
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`litongjava_windows_tts` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `litongjava_windows_tts`;

/*Table structure for table `sys_user` */

DROP TABLE IF EXISTS `sys_user`;

CREATE TABLE `sys_user` (
  `id` varchar(36) NOT NULL COMMENT '主键',
  `number` int(11) unsigned DEFAULT NULL COMMENT '编号',
  `login_name` varchar(100) DEFAULT NULL COMMENT '登录名',
  `password` varchar(100) DEFAULT NULL COMMENT '密码',
  `nick_name` varchar(100) DEFAULT NULL COMMENT '昵称',
  `register_ip` varchar(255) DEFAULT NULL COMMENT '注册ip',
  `is_del` char(1) DEFAULT '0' COMMENT '是否删除,1删除,0未删除',
  `create_by` varchar(64) DEFAULT NULL COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT NULL COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remarks` varchar(255) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户表';

/*Data for the table `sys_user` */

insert  into `sys_user`(`id`,`number`,`login_name`,`password`,`nick_name`,`register_ip`,`is_del`,`create_by`,`create_time`,`update_by`,`update_time`,`remarks`) values ('afce722c6dd849ab97bc59b4b43bc1bf',1,'admin','c7540eb7e65b553ec1ba6b20de79608',NULL,NULL,'0',NULL,'2020-08-29 21:59:07',NULL,'2020-08-29 21:59:07',NULL);

/*Table structure for table `tts_file` */

DROP TABLE IF EXISTS `tts_file`;

CREATE TABLE `tts_file` (
  `file_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `record_id` int(11) unsigned NOT NULL COMMENT '记录id',
  `voice` varchar(255) NOT NULL COMMENT '语音库名称',
  `type` int(11) NOT NULL COMMENT '音频的输出格式',
  `volume` int(11) NOT NULL COMMENT '声音：1到100',
  `rate` int(11) NOT NULL COMMENT '频率：-10到10',
  `file_path` varchar(255) NOT NULL COMMENT '文件路径,相对路径',
  `sort` int(11) unsigned NOT NULL DEFAULT 0 COMMENT '排序方式(数字越小越靠前)',
  `delete_time` datetime DEFAULT NULL COMMENT '软删除',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`file_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

/*Data for the table `tts_file` */

insert  into `tts_file`(`file_id`,`record_id`,`voice`,`type`,`volume`,`rate`,`file_path`,`sort`,`delete_time`,`create_time`,`update_time`) values (1,1,'Microsoft Huihui Desktop - Chinese (Simplified)',6,100,0,'file/auido/Microsoft Huihui Desktop - Chinese (Simplified)__6_100_0_3c9a428247afebedcf353149379f0066.wav',0,NULL,'2020-09-02 09:02:10','2020-09-02 09:02:10'),(2,2,'Microsoft Huihui Desktop - Chinese (Simplified)',6,100,0,'file/auido/Microsoft Huihui Desktop - Chinese (Simplified)__6_100_0_99c7c34c6486607d070e89ef4104cdad.wav',0,NULL,'2020-09-02 09:02:30','2020-09-02 09:02:30'),(3,3,'Microsoft Huihui Desktop - Chinese (Simplified)',6,100,0,'file/auido/Microsoft Huihui Desktop - Chinese (Simplified)__6_100_0_7c343a01d8961d2912ad5ce50756b211.wav',0,NULL,'2020-09-02 09:54:02','2020-09-02 09:54:02'),(4,4,'Microsoft Huihui Desktop - Chinese (Simplified)',6,100,0,'file/auido/Microsoft Huihui Desktop - Chinese (Simplified)__6_100_0_d2b93125dec3da4392c16cdf2f6cea3a.wav',0,NULL,'2020-09-02 10:06:13','2020-09-02 10:06:13'),(5,5,'Microsoft Huihui Desktop - Chinese (Simplified)',6,100,0,'file/auido/Microsoft Huihui Desktop - Chinese (Simplified)__6_100_0_092cc6db886242178cc2dcb3389bfdb1.wav',0,NULL,'2020-09-02 10:06:56','2020-09-02 10:06:56');

/*Table structure for table `tts_record` */

DROP TABLE IF EXISTS `tts_record`;

CREATE TABLE `tts_record` (
  `record_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `content` text NOT NULL COMMENT '合成文字',
  `sort` int(11) unsigned NOT NULL DEFAULT 0 COMMENT '排序方式(数字越小越靠前)',
  `delete_time` datetime DEFAULT NULL COMMENT '软删除',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`record_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

/*Data for the table `tts_record` */

insert  into `tts_record`(`record_id`,`content`,`sort`,`delete_time`,`create_time`,`update_time`) values (1,'好了，正文开始。前段时间阿里云搞活动，于是就买了一台 ESC 云服务器，作为自己的平时学习各种技术和项目部署的机器。但是随着项目和技术的更新，一些技术是需要安装相关的服务，比如缓存服务 redis、数据库mysql、消息队列 kafka、Web服务 nginx（openresty）、分布式协调服务 zookkeeper、分布式搜索服务 elasticsearch、大数据相关实时流计算服务 storm 以及自己项目部署等等等的一系列的服务，它们都是需要安装与部署。然而服务的安装部署是需要内存的！本人买的 ESC 机器的内存很小，只有 2G（因为穷(^_^)买不起高配置的机器）！显然 2G 的内存是不够的。',0,NULL,'2020-09-02 09:02:10','2020-09-02 09:02:10'),(2,'我是中国人,我爱我的祖国',0,NULL,'2020-09-02 09:02:30','2020-09-02 09:02:30'),(3,'  秦孝公据崤函之固，拥雍州之地，君臣固守以窥周室，有席卷天下，包举宇内，囊括四海之意，并吞八荒之心。当是时也，商君佐之，内立法度，务耕织，修守战之具，外连衡而斗诸侯。于是秦人拱手而取西河之外。孝公既没，惠文、武、昭襄蒙故业，因遗策，南取汉中，西举巴、蜀，东割膏腴之地，北收要害之郡。诸侯恐惧，会盟而谋弱秦，不爱珍器重宝肥饶之地，以致天下之士，合从缔交，相与为一。当此之时，齐有孟尝，赵有平原，楚有春申，魏有信陵。此四君者，皆明智而忠信，宽厚而爱人，尊贤而重士，约从离衡，兼韩、魏、燕、楚、齐、赵、宋、卫、中山之众。于是六国之士，有宁越、徐尚、苏秦、杜赫之属为之谋，齐明、周最、陈轸、召滑、楼缓、翟景、苏厉、乐毅之徒通其意，吴起、孙膑、带佗、倪良、王廖、田忌、廉颇、赵奢之伦制其兵。尝以十倍之地，百万之众，叩关而攻秦。秦人开关延敌，九国之师，逡巡而不敢进。秦无亡矢遗镞之费，而天下诸侯已困矣。于是从散约败，争割地而赂秦。秦有余力而制其弊，追亡逐北，伏尸百万，流血漂橹；因利乘便，宰割天下，分裂山河。强国请服，弱国入朝。延及孝文王、庄襄王，享国之日浅，国家无事。及至始皇，奋六世之余烈，振长策而御宇内，吞二周而亡诸侯，履至尊而制六合，执敲扑而鞭笞天下，威振四海。南取百越之地，以为桂林、象郡；百越之君，俯首系颈，委命下吏。乃使蒙恬北筑长城而守藩篱，却匈奴七百余里；胡人不敢南下而牧马，士不敢弯弓而报怨。于是废先王之道，焚百家之言，以愚黔首；隳名城，杀豪杰；收天下之兵，聚之咸阳，销锋镝，铸以为金人十二，以弱天下之民。然后践华为城，因河为池，据亿丈之城，临不测之渊，以为固。良将劲弩守要害之处，信臣精卒陈利兵而谁何。天下已定，始皇之心，自以为关中之固，金城千里，子孙帝王万世之业也。始皇既没，余威震于殊俗。然陈涉瓮牖绳枢之子，氓隶之人，而迁徙之徒也；才能不及中人，非有仲尼，墨翟之贤，陶朱、猗顿之富；蹑足行伍之间，而倔起阡陌之中，率疲弊之卒，将数百之众，转而攻秦；斩木为兵，揭竿为旗，天下云集响应，赢粮而景从。山东豪俊遂并起而亡秦族矣。且夫天下非小弱也，雍州之地，崤函之固，自若也。陈涉之位，非尊于齐、楚、燕、赵、韩、魏、宋、卫、中山之君也；锄櫌棘矜，非铦于钩戟长铩也；谪戍之众，非抗于九国之师也；深谋远虑，行军用兵之道，非及向时之士也。然而成败异变，功业相反，何也？试使山东之国与陈涉度长絜大，比权量力，则不可同年而语矣。然秦以区区之地，致万乘之势，序八州而朝同列，百有余年矣；然后以六合为家，崤函为宫；一夫作难而七庙隳，身死人手，为天下笑者，何也？仁义不施而攻守之势异也。',0,NULL,'2020-09-02 09:54:02','2020-09-02 09:54:02'),(4,'你好什么名字',0,NULL,'2020-09-02 10:06:13','2020-09-02 10:06:13'),(5,'1、如何使用js控制修改audio的src或它的source 的src属性实现动态改变audio放的音频？a、只有修改audio 的src值，才可以播放修改source 的 src值却不可以播放b、在更改src后要加上load();函数（这是js下的函数，不是jquery的那个load()）',0,NULL,'2020-09-02 10:06:56','2020-09-02 10:06:56');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
