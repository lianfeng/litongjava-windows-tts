package com.litong.modules.tts.controller;

import java.io.File;

import com.jfinal.aop.Inject;
import com.jfinal.core.Controller;
import com.jfinal.core.paragetter.Para;
import com.jfinal.kit.StrKit;
import com.litong.modules.tts.service.TTSSerivce;
import com.litong.modules.tts.vo.TTSVo;
import com.litong.utils.vo.JsonBean;

import lombok.extern.slf4j.Slf4j;

/**
 * @author bill robot
 * @date 2020年8月16日_下午11:43:15 
 * @version 1.0 
 * @desc
 */
@Slf4j
public class TTSController extends Controller {
  @Inject
  private TTSSerivce ttsService;

  public void test(@Para("") TTSVo ttsVo) {
    renderJson(ttsVo);
  }

  /**
   * 获取当前发音人
   */
  public void getCurrentVoice() {
    renderJson(ttsService.getCurrentVoice());
  }

  /**
   * 清楚所有缓存变量
   */
  public void clear() {
    ttsService.clearCurrentVoice();
    renderJson("success");
  }

  public void index(@Para("") TTSVo ttsVo) {
    log.info("ttsVo:{}", ttsVo);
    String text = ttsVo.getText();
    if(StrKit.isBlank(text)) {
      renderText("content not be null");
      return;
    }
    int type = ttsVo.getType();
    if (type == 0) {
      ttsVo.setType(6);
    }
    int volume = ttsVo.getVolume();
    if (volume == 0) {
      ttsVo.setVolume(100);
    }
    // 设置翻发音人需要修改注册表,我放弃修改了;
    ttsVo.setVoice(ttsService.getCurrentVoice());
    File file = null;
    try {
      file = ttsService.tts(ttsVo);
    } catch (Exception e) {
      e.printStackTrace();
      JsonBean<Void> jsonBean = new JsonBean<Void>(e.getMessage());
      renderJson(jsonBean);
      return;
    }
    renderFile(file);
  }
}
