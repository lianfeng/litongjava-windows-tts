package com.litong.modules.tts.service;

import java.io.File;

import com.jacob.activeX.ActiveXComponent;
import com.jacob.com.Dispatch;
import com.jacob.com.Variant;
import com.jfinal.kit.HashKit;
import com.litong.modules.tts.vo.TTSVo;
import com.litong.utils.dll.LibraryUtil;

/**
 * MS 语音合成工具类
 */
//@Slf4j
public class MsTtsUtil {

  public static void main(String[] args) {
    LibraryUtil.addLibary();
    String parentPath = "D:/dev_workspace/java/litong-project/litongjava-windows-tts/target/classes/litongjava-windows-tts/file/auido";
    File file = new File(parentPath);
    if (!file.exists()) {
      file.mkdirs();
    }
    TTSVo ttsVo = new TTSVo();
    ttsVo.setType(6);
    ttsVo.setVolume(100);
    ttsVo.setRate(0);
    ttsVo.setText("今天猪肉又涨价了,一下子涨了两块钱");
    ttsToFile(ttsVo, parentPath);
    ttsVo.setText("今天羊肉羊肉也涨价了");
    ttsToFile(ttsVo, parentPath);
    // String path = "text\\过秦论.txt";
    // textToFile(path);
  }

  /**
   * 获取当前发音人
   */
  public static String getCurrentVoice() {
    // 创建与微软应用程序的新连接。传入的参数是注册表中注册的程序的名称。相当实例化Sapi.SpVoice对象,但是ax并不是Sapi.SpVoice,ax是包装信息,ax包含Sapi.SpVoice
    ActiveXComponent ax = new ActiveXComponent("Sapi.SpVoice");
    String currentVoice = getCurrentVoice(ax);
    ax.safeRelease();
    return currentVoice;
  }

  private static String getCurrentVoice(ActiveXComponent ax) {
    // 获取Sapi.SpVoice对象的voice属性值,voice的属性值是一个voice对象,对象使用Dispatch包装
    Dispatch voiceDispatche = ax.getProperty("voice").toDispatch();
    // 执行voice对象的GetDescription方法,返回值可能使用String,使用Variant包装
    Variant descripton = Dispatch.call(voiceDispatche, "GetDescription");
    return descripton.toString();
  }

  /**
   * 语音转文字音频
   * 
   * @param txt
   */
  public static String ttsToFile(TTSVo ttsVo, String parentPath) {
    ActiveXComponent ax = new ActiveXComponent("Sapi.SpVoice");
    String filePath = getFilePath(ax, ttsVo, parentPath);
    try {
      // 运行时输出语音内容
      Dispatch spVoice = ax.getObject();
      // 下面是构建文件流到成语音文件
      ax = new ActiveXComponent("Sapi.SpFileStream");
      Dispatch spFileStream = ax.getObject();

      ax = new ActiveXComponent("Sapi.SpAudioFormat");
      Dispatch spAudioFormat = ax.getObject();

      // 设置音频流格式
      Dispatch.put(spAudioFormat, "Type", ttsVo.getType());
      // 设置文件输出流格式
      Dispatch.putRef(spFileStream, "Format", spAudioFormat);
      // 调用输出 文件流打开方法，创建一个.wav文件
      Dispatch.call(spFileStream, "Open", new Variant(filePath), new Variant(3), new Variant(true));
      // 设置声音对象的音频输出流为输出文件对象
      Dispatch.putRef(spVoice, "AudioOutputStream", spFileStream);
      // 设置音量 0到100
      Dispatch.put(spVoice, "Volume", new Variant(ttsVo.getVolume()));
      // 设置朗读速度
      Dispatch.put(spVoice, "Rate", new Variant(ttsVo.getRate()));
      // 开始朗读,实际上是合成得到文件
      Dispatch.call(spVoice, "Speak", new Variant(ttsVo.getText()));

      // 关闭输出文件
      Dispatch.call(spFileStream, "Close");
      Dispatch.putRef(spVoice, "AudioOutputStream", null);

      spAudioFormat.safeRelease();
      spFileStream.safeRelease();
      spVoice.safeRelease();
    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      ax.safeRelease();
    }
    return filePath;
  }

  public static String getFilePath(ActiveXComponent ax, TTSVo ttsVo, String parentPath) {
    // 文件名规则
    // voice_type_volume_rate_md5

    String voice = null;
    if (ttsVo.getVoice() == null) {
      voice = getCurrentVoice(ax);
    } else {
      voice = ttsVo.getVoice();
    }

    String filename = append("_", ttsVo.getType(), ttsVo.getVolume(), ttsVo.getRate());
    // 添加文件名称
    filename += "_" + HashKit.md5(ttsVo.getText());
    // 添加文件后缀
    if (ttsVo.getType() == 6) {
      filename += ".wav";
    }
    // 发音人和加目录
    filename = parentPath + File.separator + voice + "_" + filename;
    return filename;
  }

  private static String append(String string, int... ints) {
    String retval = new String();
    for (int i : ints) {
      retval += "_" + i;
    }
    return retval;
  }
}