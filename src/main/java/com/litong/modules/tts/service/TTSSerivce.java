package com.litong.modules.tts.service;

import java.io.File;
import java.util.Date;

import org.apache.commons.io.FilenameUtils;

import com.litong.modules.tts.common.model.TtsFile;
import com.litong.modules.tts.common.model.TtsRecord;
import com.litong.modules.tts.vo.TTSVo;

import lombok.extern.slf4j.Slf4j;

/**
 * @author bill robot
 * @date 2020年9月1日_下午6:28:39 
 * @version 1.0 
 * @desc    
 */
@Slf4j
public class TTSSerivce {

  private volatile String currentVoice = null;
  private Object lock = new Object();
  private TtsFile ttsFileDao = new TtsFile().dao();
  private TtsRecord ttsRecordDao = new TtsRecord().dao();

  /**
   * 查询数据库,如果没有记录,合成文件并记录数据库
   * @param ttsVo
   * @return
   */
  public File tts(TTSVo ttsVo) {
    // 1.查询数据库
    String sql = "select record_id from tts_record where `delete_time` IS NULL and content=? ";
    TtsRecord ttsRecord = ttsRecordDao.findFirst(sql, ttsVo.getText());
    if (ttsRecord == null) {
      String ttsToFile = ttsToFile(ttsVo, null);
      return new File(ttsToFile);
    }

    Long recordId = ttsRecord.getRecordId();
    sql = "select file_path from tts_file where `delete_time` IS NULL and record_id=? and voice=? and type=? and volume=? and rate=?";
    TtsFile ttsFile = ttsFileDao.findFirst(sql, recordId, ttsVo.getVoice(), ttsVo.getType(), ttsVo.getVolume(), ttsVo.getRate());

    // 2.判断文件是否存在
    if (ttsFile == null) {
      String ttsToFile = ttsToFile(ttsVo, recordId);
      return new File(ttsToFile);
    }
    File audioFile = new File(ttsFile.getFilePath());
    // 3.文件存在返回
    if (audioFile.exists()) {
      log.info("使用之前合成的文件:{}", audioFile.getName());
      return audioFile;
    }
    String ttsToFile = ttsToFile(ttsVo, recordId);
    return new File(ttsToFile);
  }

  /**
   * 合成文件并记录数据库  
   * @param ttsVo
   * @param recordId
   * @return
   */
  private String ttsToFile(TTSVo ttsVo, Long recordId) {
    // 4.文件不存在
    String parentPath = "file/auido";
    File file = new File(parentPath);
    if (!file.exists()) {
      file.mkdirs();
      log.info("mkdir:{}", file.getAbsoluteFile());
    }
    // 5.合成文件
    String ttsToFile = MsTtsUtil.ttsToFile(ttsVo, parentPath);
    log.info("audio file:{}", ttsToFile);
    // 6.保存到数据库
    // 为了方便迁移,仅仅将文件静态目录后的信息保存到数据库
    String name = FilenameUtils.getName(ttsToFile);
    Date now = new Date();
    if (recordId == null) {
      TtsRecord ttsRecord = new TtsRecord();
      ttsRecord.setContent(ttsVo.getText());
      ttsRecord.setCreateTime(now);
      ttsRecord.setUpdateTime(now);
      ttsRecord.save();
      recordId = ttsRecord.getRecordId();
    }
    TtsFile ttsFile = new TtsFile();
    ttsFile.setRecordId(recordId);
    ttsFile.setVoice(ttsVo.getVoice());
    ttsFile.setType(ttsVo.getType());
    ttsFile.setVolume(ttsVo.getVolume());
    ttsFile.setRate(ttsVo.getRate());
    ttsFile.setFilePath(parentPath + "/" + name);
    ttsFile.setCreateTime(now);
    ttsFile.setUpdateTime(now);
    ttsFile.save();
    // 7.返回
    return ttsToFile;
  }

  public String getCurrentVoice() {
    if (currentVoice == null) {
      String s1 = MsTtsUtil.getCurrentVoice();
      synchronized (lock) {
        if (currentVoice == null) {
          this.currentVoice = s1;
        }
      }
    }
    return currentVoice;
  }

  public void clearCurrentVoice() {
    this.currentVoice = null;
  }

}
