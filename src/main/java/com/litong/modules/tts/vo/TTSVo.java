package com.litong.modules.tts.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author bill robot
 * @date 2020年8月16日_下午11:46:33 
 * @version 1.0 
 * @desc
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TTSVo {
  private String voice,text;
  private int type, volume, rate;
  

}
