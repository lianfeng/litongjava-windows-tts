package com.litong.jfinal.route;

import com.jfinal.config.Routes;
import com.litong.jfinal.controler.ApiFormController;
import com.litong.jfinal.controler.ApiRoleController;
import com.litong.modules.tts.controller.TTSController;

/**
 * @author bill robot
 * @date 2020年8月16日_下午5:08:54 
 * @version 1.0 
 * @desc
 */
public class ApiRoutes extends Routes {

  @Override
  public void config() {
    add("tts", TTSController.class);
    add("api/form", ApiFormController.class);
    add("api/users", ApiUsersController.class);
    add("api/role",ApiRoleController.class);
  }

}
