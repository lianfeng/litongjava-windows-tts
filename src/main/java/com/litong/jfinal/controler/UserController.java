package com.litong.jfinal.controler;

import com.jfinal.aop.Inject;
import com.jfinal.core.Controller;
import com.jfinal.kit.Kv;
import com.litong.jfinal.service.UserService;
import com.litong.utils.aes.AESCrtUtils;
import com.litong.utils.digest.MD5Util;
import com.litong.utils.digest.SHA1Util;
import com.litong.utils.vo.JsonBean;

import lombok.extern.slf4j.Slf4j;

/**
 * @author bill robot
 * @date 2020年8月29日_下午5:32:28 
 * @version 1.0 
 * @desc
 */
@Slf4j
public class UserController extends Controller{
  
  @Inject
  private UserService userService;
  
  /**
   * 添加用户
   * @param kv
   */
  public void add(Kv kv) {
    log.info("kv:{}", kv);
    String loginName = kv.getStr("loginName");
    String password = kv.getStr("password");
    String key = kv.getStr("key");
    //明文密码
    String plainText = AESCrtUtils.decrypt(password, key);
    //密文密码
    String encodeText = MD5Util.encode(SHA1Util.encode(plainText));
    boolean b = userService.add(loginName,encodeText);
    renderJson(new JsonBean<Boolean>(b));
  }

}
